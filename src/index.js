import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { FormLogin } from "react-bootstrap";

// import { Tables } from "react-bootstrap";

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
