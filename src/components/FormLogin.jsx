import React, { Component } from "react";
import { Button, Form, ButtonGroup, Row } from "react-bootstrap";
class FormLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",

      username: "",
      usernameErr: "",
      emailErr: "",
      gender: "male",
      job: "student",
    };
  }
  onHandleText = (e) => {
    console.log("e:", e.target.name);

    // let name="password"

    this.setState(
      {
        [e.target.name]: e.target.value,
      },
      () => {
        console.log(this.state);
        if (e.target.name === "username") {
          let pattern3 = /^[\w\s-]{3,13}$/g;
          let result3 = pattern3.test(this.state.username);
          if (result3) {
            this.setState({
              usernameErr: "",
            });
          } else if (this.state.username === "") {
            this.setState({
              usernameErr: "Username cannot be empty",
            });
          } else {
            this.setState({
              usernameErr:
                " No more than 9 characters, No less than 3 characters",
            });
          }
        }
        if (e.target.name === "email") {
          let pattern = /^\S+@\S+\.[a-z]{3}$/g;
          let result = pattern.test(this.state.email.trim());
          if (result) {
            this.setState({
              emailErr: "",
            });
          } else if (this.state.email === "") {
            this.setState({
              emailErr: "Email cannot be empty!",
            });
          } else {
            this.setState({
              emailErr: "Email is invalid!",
            });
          }
        }
      }
    );
  };

  validateBtn = () => {
    if (
      this.state.usernameErr === "" &&
      this.state.emailErr === "" &&
      this.state.username !== "" &&
      this.state.email !== ""
    ) {
      return false;
    } else {
      return true;
    }
  };
  render() {
    return (
      <Row>
        <Form className="form">
          <h1>Persional Info</h1>

          <Form.Group controlId="formBasicEmail">
            <Form.Label>User Name</Form.Label>
            <Form.Control
              onChange={this.onHandleText}
              name="username"
              type="username"
              placeholder="Enter Username"
            />
            <Form.Text className="text-muted">
              <p style={{ color: "red" }}>
                {this.state.usernameErr}
                {this.state.result3}
              </p>
            </Form.Text>
          </Form.Group>

          <Form.Group>
            <Form.Label>Email</Form.Label>
            <Form.Control
              name="email"
              onChange={this.onHandleText}
              type="email"
              placeholder="Email"
            />
            <Form.Text className="text-muted">
              <p style={{ color: "red" }}>{this.state.emailErr}</p>
            </Form.Text>
          </Form.Group>
          <div className="btn">
            <Button
              disabled={this.validateBtn()}
              variant="primary"
              type="submit"
            >
              Submit
            </Button>
            <Button className="right" variant="secondary" type="submit">
              Cencel
            </Button>
          </div>
        </Form>
        <Form className="radio" sm={5}>
          <h3>Gender</h3>

          <Form.Check
            inline
            label="Male"
            name="gender"
            type="radio"
            id="mail"
            defaultChecked={this.state.gender === "male" ? true : false}
          />

          <Form.Check
            inline
            label="Female"
            name="gender"
            type="radio"
            id="female"
            defaultChecked={this.state.gender === "female" ? true : false}
          />
          <h4>Job</h4>

          <Form.Check
            inline
            label="Student"
            name="gender"
            type="checkbox"
            id="mail"
            defaultChecked={this.state.job === "student" ? true : false}
          />

          <Form.Check
            inline
            label="Teacher"
            name="gender"
            type="checkbox"
            id="Female"
            defaultChecked={this.state.job === "teacher" ? true : false}
          />
          <Form.Check
            inline
            label="Developer"
            name="gender"
            type="checkbox"
            id="Female"
            defaultChecked={this.state.job === "Developer" ? true : false}
          />
        </Form>
      </Row>
    );
  }
}

export default FormLogin;
