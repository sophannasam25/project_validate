import "./App.css";
import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import FormLogin from "./components/FormLogin";
import { Container, Row, Col } from "react-bootstrap";
import Radio from "./components/Radio";

function App() {
  return (
    <Container>
      <Row>
        <Col sm={12}>
          <FormLogin />
        </Col>
      </Row>
    </Container>
  );
}

export default App;
